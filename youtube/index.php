<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="awesome videos!">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/jumbotron.css" rel="stylesheet">
    <title>Youtube Viral Search</title>
    <style>
    body {
        height: 100%;
        margin: 30px;
    }
    </style>
</head>

<body>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
		<a class="navbar-brand" href="../index.html">Home</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault"
		    aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarsExampleDefault">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="../gmaps.php">Google Maps API</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="../youtube">Youtube API</a>
				</li>
				<li class="nav-item">
					<a class="nav-link disabled" href="../sc_404_err.html">Soundcloud API</a>
				</li>
			</ul>
		</div>
    </nav>
    <h1>Youtube Video Searcher V.1</h1>
    <div class="">
        <form id="form" action="#">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Type om te zoeken op YouTube..." autocomplete="off">
            </div>
            <button class="btn btn-default" type="submit">Zoek &raquo;</button>
        </form>
    </div>
</br><p>In de Console komt een error als je een zoekopdracht uitvoerd die wij niet konden oplossen, we waren ook te laat aan deze API begonnen om nog om hulp te vragen.<p>

    <!-- scripts -->
    <script src="https://apis.google.com/js/client.js?onload=init"></script>
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="https://apis.google.com/js/client.js?onload=googleApiClientReady"></script>
    <script src="js/app.js"></script>
</body>

</html>