<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
		<meta charset="utf-8">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" rel="stylesheet">
		<link href="css/jumbotron.css" rel="stylesheet">
		<link href="css/gmaps.css" rel="stylesheet">
		<title>Google Maps API</title>
	</head>
	<body>
	<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
		<a class="navbar-brand" href="index.html">Home</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarsExampleDefault">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="gmaps.php">Google Maps API
						<span class="sr-only">(current)</span>
					</a>
					<li class="nav-item">
						<a class="nav-link" href="youtube">Youtube API</a>
					</li>
					<li class="nav-item">
						<a class="nav-link disabled" href="sc_404_err.html">Soundcloud API</a>
					</li>
				</li>
			</ul>
		</div>
	</nav>
		<div id="map"></div>
		<div id="right-panel">
		<div>
		<b>Vertrekpunt:</b>
		<select id="start">
		<option value="'s-Hertogenbosch, NL">'s-Hertogenbosch</option>
		<option value="Amsterdam, NL">Amsterdam</option>
		<option value="Assen, NL">Assen</option>
		<option value="Den Haag, NL">Den Haag</option>
		<option value="Eindhoven, NL">Eindhoven</option>
		<option value="Emmen, NL">Emmen</option>
		<option value="Groningen, NL">Groningen</option>
		<option value="Nieuwerkerk, NL">Nieuwerkerk</option>
		<option value="Rotterdam, NL">Rotterdam</option>
		<option value="Utrecht, NL">Utrecht</option>
		<option value="Vlissingen, NL">Vlissingen</option>
		<option value="Zierikzee, NL">Zierikzee</option>
		</select>
		<br><br>
		<b>Tussenstop(s):</b> <br>
		<i>(<b>CTRL+CLICK</b> / <b>CMD+CLICK</b> om meerdere te selecteren)</i><br>
		<select multiple id="waypoints">
		<option value="'s-Hertogenbosch, NL">'s-Hertogenbosch</option>
		<option value="Amsterdam, NL">Amsterdam</option>
		<option value="Assen, NL">Assen</option>
		<option value="Den Haag, NL">Den Haag</option>
		<option value="Eindhoven, NL">Eindhoven</option>
		<option value="Emmen, NL">Emmen</option>
		<option value="Groningen, NL">Groningen</option>
		<option value="Engeland, UK">Groot-Brittannië (My City)</option>
		<option value="Nieuwerkerk, NL">Nieuwerkerk</option>
		<option value="Rotterdam, NL">Rotterdam</option>
		<option value="Utrecht, NL">Utrecht</option>
		<option value="Vlissingen, NL">Vlissingen</option>
		<option value="Zierikzee, NL">Zierikzee</option>
		</select>
		<br><br>
		<b>Bestemming:</b>
		<select id="end">
		<option value="'s-Hertogenbosch, NL">'s-Hertogenbosch</option>
		<option value="Amsterdam, NL">Amsterdam</option>
		<option value="Assen, NL">Assen</option>
		<option value="Den Haag, NL">Den Haag</option>
		<option value="Eindhoven, NL">Eindhoven</option>
		<option value="Emmen, NL">Emmen</option>
		<option value="Groningen, NL">Groningen</option>
		<option value="Nieuwerkerk, NL">Nieuwerkerk</option>
		<option value="Rotterdam, NL">Rotterdam</option>
		<option value="Utrecht, NL">Utrecht</option>
		<option value="Vlissingen, NL">Vlissingen</option>
		<option value="Zierikzee, NL">Zierikzee</option>
		</select>
		<br>
		<br>
			<input type="submit" id="submit" class="btn btn-default buttonw">
		</div>
		<div id="directions-panel"></div>
		</div>
		<script src="js/gmaps.js"></script>
		<script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxyTQNWNsuGXguuGayelIGr00UUOvXKsE&callback=initMap">
		</script>
	</body>
</html>